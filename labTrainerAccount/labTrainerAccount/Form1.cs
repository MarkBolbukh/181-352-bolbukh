﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labTrainerAccount
{
    public partial class Form1 : Form
    {
        private games g;
        public Form1()
        {
            InitializeComponent();
            //
            g = new games();
            g.Change += Event_Change;
            g.DoReset();
            //
            btYes.Click += (sender, e) => g.DoAnswer(true);
            btNo.Click += (sender, e) => g.DoAnswer(false);

        }
        private void Event_Change(object sender, EventArgs e)
        {
            labelVerno.Text = string.Format("Верно = {0}", g.CountCorrect.ToString());
            labelNeVerno.Text = string.Format("Не Верно = {0}", g.CountWrong.ToString());
            labelPrimer.Text = g.CodeText;
        }

    }
}
