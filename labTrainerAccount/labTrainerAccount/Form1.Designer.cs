﻿namespace labTrainerAccount
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.labelVerno = new System.Windows.Forms.Label();
            this.labelNeVerno = new System.Windows.Forms.Label();
            this.btYes = new System.Windows.Forms.Button();
            this.btNo = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.labelPrimer = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.labelNeVerno, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelVerno, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(35, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(416, 50);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.btNo, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.btYes, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(35, 299);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(416, 100);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // labelVerno
            // 
            this.labelVerno.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelVerno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelVerno.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelVerno.Location = new System.Drawing.Point(3, 0);
            this.labelVerno.Name = "labelVerno";
            this.labelVerno.Size = new System.Drawing.Size(202, 50);
            this.labelVerno.TabIndex = 0;
            this.labelVerno.Text = "Верно = 0";
            this.labelVerno.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelNeVerno
            // 
            this.labelNeVerno.BackColor = System.Drawing.SystemColors.ControlDark;
            this.labelNeVerno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelNeVerno.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelNeVerno.Location = new System.Drawing.Point(211, 0);
            this.labelNeVerno.Name = "labelNeVerno";
            this.labelNeVerno.Size = new System.Drawing.Size(202, 50);
            this.labelNeVerno.TabIndex = 1;
            this.labelNeVerno.Text = "Не верно = 0";
            this.labelNeVerno.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btYes
            // 
            this.btYes.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btYes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btYes.Font = new System.Drawing.Font("Microsoft Sans Serif", 35F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btYes.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.btYes.Location = new System.Drawing.Point(3, 3);
            this.btYes.Name = "btYes";
            this.btYes.Size = new System.Drawing.Size(202, 94);
            this.btYes.TabIndex = 0;
            this.btYes.Text = "Да";
            this.btYes.UseVisualStyleBackColor = false;
            // 
            // btNo
            // 
            this.btNo.BackColor = System.Drawing.SystemColors.GrayText;
            this.btNo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 35F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btNo.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btNo.Location = new System.Drawing.Point(211, 3);
            this.btNo.Name = "btNo";
            this.btNo.Size = new System.Drawing.Size(202, 94);
            this.btNo.TabIndex = 1;
            this.btNo.Text = "Нет";
            this.btNo.UseVisualStyleBackColor = false;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(35, 259);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(416, 38);
            this.label3.TabIndex = 2;
            this.label3.Text = "Верно ?";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPrimer
            // 
            this.labelPrimer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelPrimer.Font = new System.Drawing.Font("Microsoft Sans Serif", 45F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelPrimer.Location = new System.Drawing.Point(32, 74);
            this.labelPrimer.Name = "labelPrimer";
            this.labelPrimer.Size = new System.Drawing.Size(416, 159);
            this.labelPrimer.TabIndex = 3;
            this.labelPrimer.Text = "10 + 10 = 26";
            this.labelPrimer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 411);
            this.Controls.Add(this.labelPrimer);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MinimumSize = new System.Drawing.Size(500, 450);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label labelNeVerno;
        private System.Windows.Forms.Label labelVerno;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button btNo;
        private System.Windows.Forms.Button btYes;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelPrimer;
    }
}

