﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labWebBrowser
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            //
            buttonGo.Click += (sender, e) => web.Navigate(textBox1.Text);
            buttonBack.Click += (sender, e) => web.GoBack();
            buttonForward.Click += (sender, e) => web.GoForward();
            buttonReload.Click += (sender, e) => web.Refresh();
            buttonSkip.Click += (sender, e) => web.Stop();
            web.DocumentCompleted += (sender, e) => textBox1.Text = web.Url.ToString();
        }


    }
}
