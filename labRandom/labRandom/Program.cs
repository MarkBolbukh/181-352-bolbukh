﻿using System;

namespace labRandom
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            for (int i = 0; i < 10; i++)
                Console.WriteLine(rnd.Next());
            Console.WriteLine();
            //
            byte[] x = new byte[15];
            rnd.NextBytes(x);
            for (int i = 0; i < x.Length; i++)
                Console.WriteLine(x[i]);
        }
    }
}
