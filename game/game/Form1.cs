﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace game
{
    public partial class Form1 : Form
    {
        public
             Label firstClicked = null;
             Label secondClicked = null;
             DateTime date;
             Timer timer = new Timer();



        public Form1()
        {
            InitializeComponent();
        }


        Random random = new Random();

        List<string> icons = new List<string>()
        {
            "!", "!", "N", "N", ",", ",", "k", "k",
            "b", "b", "v", "v", "w", "w", "z", "z"

        };

        private void AssignIconsToSquares()
        {

            foreach (Control control in tableLayoutPanel1.Controls)
            {
                Label iconLabel = control as Label;
                if (iconLabel != null)
                {
                    int randomNumber = random.Next(icons.Count);
                    iconLabel.Text = icons[randomNumber];
                    iconLabel.ForeColor = iconLabel.BackColor;
                    icons.RemoveAt(randomNumber);
                }
            }
        }

        private void label_Click(object sender, EventArgs e)
        {

            if (timer1.Enabled == true)
                return;

            Label clickedLabel = sender as Label;

            if (secondClicked != null)
                return;

            if (clickedLabel != null)
            {
                if (clickedLabel.ForeColor == Color.Black)
                    return;

                if (firstClicked == null)
                {
                    firstClicked = clickedLabel;
                    firstClicked.ForeColor = Color.Black;
                    return;
                }

                secondClicked = clickedLabel;
                secondClicked.ForeColor = Color.Black;
               
                secondClicked = clickedLabel;
                secondClicked.ForeColor = Color.Black;

                CheckForWinner();

                if (firstClicked.Text == secondClicked.Text)
                {
                    firstClicked = null;
                    secondClicked = null;
                    return;
                }

                timer1.Start();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();

            firstClicked.ForeColor = firstClicked.BackColor;
            secondClicked.ForeColor = secondClicked.BackColor;

            firstClicked = null;
            secondClicked = null;
        }

        private void CheckForWinner()
        {

            foreach (Control control in tableLayoutPanel1.Controls)
            {
                Label iconLabel = control as Label;

                if (iconLabel != null)
                {
                    if (iconLabel.ForeColor == iconLabel.BackColor)
                        return;
                }
            }
            timer.Stop();
            MessageBox.Show("Ты собрал все пары!");
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AssignIconsToSquares();
            tableLayoutPanel1.Visible = true;
            button1.Visible = false;
            button2.Visible = true;
            label17.Visible = true;
            date = DateTime.Now;
        
            timer.Interval = 10;
            timer.Tick += new EventHandler(tickTimer);
            timer.Start();

        }

        private void tickTimer(object sender,EventArgs e)
        {
            long tick = DateTime.Now.Ticks - date.Ticks;
            DateTime stopwatch = new DateTime();

            stopwatch = stopwatch.AddTicks(tick);
            label17.Text = string.Format("{0:HH:mm:ss:ff}", stopwatch);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
    }

}
